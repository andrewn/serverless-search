module gitlab.com/andrewn/serverless-search

require (
	cloud.google.com/go v0.26.0
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	google.golang.org/api v0.1.0
)
