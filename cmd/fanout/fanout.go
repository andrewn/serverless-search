package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	search "gitlab.com/andrewn/serverless-search"
	"google.golang.org/api/option"
)

var (
	inputBucket  = flag.String("input-bucket", "gitlab-gprd-logging-archive", "Bucket to search")
	prefix       = flag.String("prefix", "", "Search prefix")
	outputBucket = flag.String("output-bucket", "gitlab-log-scanner", "Bucket to search")
	outputPrefix = flag.String("output-prefix", "", "Output prefix")
	searchString = flag.String("search-string", "", "String to search")
)

func main() {
	flag.Parse()

	log.Printf("starting")

	e := search.FanoutEvent{
		InputBucket:  *inputBucket,
		Prefix:       *prefix,
		OutputBucket: *outputBucket,
		OutputPrefix: *outputPrefix,
		SearchString: *searchString,
	}
	err := run(e)
	if err != nil {
		log.Fatalf("fatal: %v", err)
	}

	log.Printf("done")
}

func run(e search.FanoutEvent) error {
	storageClient, err := storage.NewClient(context.Background(), option.WithCredentialsFile("gitlab-production-fa158569d8eb.json"))
	if err != nil {
		return fmt.Errorf("storage: new client: %v", err)
	}
	defer storageClient.Close()

	pubsubClient, err := pubsub.NewClient(context.Background(), "gitlab-production", option.WithCredentialsFile("gitlab-production-fa158569d8eb.json"))
	if err != nil {
		return fmt.Errorf("pubsub: new client: %v", err)
	}
	defer pubsubClient.Close()

	return search.Fanout(context.Background(), storageClient, pubsubClient, e)
}
