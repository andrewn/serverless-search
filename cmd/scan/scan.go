package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"cloud.google.com/go/storage"
	search "gitlab.com/andrewn/serverless-search"
	"google.golang.org/api/option"
)

var (
	inputBucket  = flag.String("input_bucket", "gitlab-gprd-logging-archive", "Bucket to search")
	inputObject  = flag.String("input-object", "", "Input object")
	outputBucket = flag.String("output-bucket", "gitlab-log-scanner", "Bucket to search")
	outputPrefix = flag.String("output-prefix", "", "Output prefix")
	searchString = flag.String("search-string", "", "String to search")
)

func main() {
	flag.Parse()

	e := search.ScanEvent{
		InputBucket:  *inputBucket,
		InputObject:  *inputObject,
		OutputBucket: *outputBucket,
		OutputPrefix: *outputPrefix,
		SearchString: *searchString,
	}

	err := run(e)
	if err != nil {
		log.Fatalf("fatal: %v", err)
	}
}

func run(e search.ScanEvent) error {
	storageClient, err := storage.NewClient(context.Background(), option.WithCredentialsFile("gitlab-production-fa158569d8eb.json"))
	if err != nil {
		return fmt.Errorf("storage: new client: %v", err)
	}
	defer storageClient.Close()

	return search.Scan(context.Background(), storageClient, e)
}
