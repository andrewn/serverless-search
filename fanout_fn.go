package search

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"cloud.google.com/go/pubsub"

	"cloud.google.com/go/storage"
)

// FanoutFunction will scan a bucket for a search string
func FanoutFunction(ctx context.Context, e Event) error {
	f := FanoutEvent{}
	err := json.Unmarshal(e.Data, &f)
	if err != nil {
		return fmt.Errorf("json: parse: %v", err)
	}

	pubsubClient, err := pubsub.NewClient(ctx, "gitlab-production")
	defer pubsubClient.Close()
	if err != nil {
		return err
	}

	storageClient, err := storage.NewClient(ctx)
	defer storageClient.Close()
	if err != nil {
		return err
	}

	err = Fanout(ctx, storageClient, pubsubClient, f)
	if err != nil {
		log.Printf("error: fanout: %v", err)
		return err
	}

	return nil
}
