package search

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/iterator"

	"cloud.google.com/go/storage"
)

// 0.8 "feels" like the right value here. Will need to test it
// to find out what the best value is. If its too high, we'll
// continue to get rate limit errors from GCP
const rateLimitPer100Seconds int64 = 107374182400 * 0.8

// Fanout will conduct a fanout search
func Fanout(ctx context.Context, storageClient *storage.Client, pubsubClient *pubsub.Client, e FanoutEvent) error {
	// See the other examples to learn how to use the Client.
	topic := pubsubClient.Topic("scan-logs")
	defer topic.Stop()

	log.Printf("Generating fanout %+v", e)

	if e.InputBucket == "" || e.Prefix == "" || e.OutputBucket == "" || e.OutputPrefix == "" || len(e.SearchString) < 3 {
		err := fmt.Errorf("fanout: invalid request: %+v", e)
		log.Printf("error: validation failure: %v", err)

		return err
	}

	it := storageClient.Bucket(e.InputBucket).Objects(ctx, &storage.Query{
		Prefix:    e.Prefix,
		Delimiter: "",
	})

	count := 0
	var quotaAllocated int64

	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return err
		}

		count++
		quotaAllocated = quotaAllocated + attrs.Size

		if quotaAllocated > rateLimitPer100Seconds {
			log.Printf("fanout: quota exceeded, awaiting for next period")
			time.Sleep(100 * time.Second)
			quotaAllocated = attrs.Size
		}

		bytes, err := json.Marshal(ScanEvent{
			InputBucket:  e.InputBucket,
			InputObject:  attrs.Name,
			OutputBucket: e.OutputBucket,
			OutputPrefix: e.OutputPrefix,
			SearchString: e.SearchString,
		})
		log.Printf("fanout: publishing %v", string(bytes))

		if err != nil {
			return err
		}

		topic.Publish(ctx, &pubsub.Message{
			Data: bytes,
		})
	}

	log.Printf("fanout: published %v messages", count)

	return nil
}
