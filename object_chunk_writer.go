package search

import (
	"context"
	"fmt"
	"io"
	"log"

	"cloud.google.com/go/storage"
)

func newChunkWriter(ctx context.Context, client *storage.Client, bucket, prefix string) io.Writer {
	return &chunkWriter{
		ctx:    ctx,
		client: client,
		bucket: bucket,
		prefix: prefix,
	}
}

type chunkWriter struct {
	ctx    context.Context
	client *storage.Client
	bucket string
	prefix string
	index  int
}

func (c *chunkWriter) Write(data []byte) (n int, err error) {
	c.index++
	fileName := fmt.Sprintf("%s.%d", c.prefix, c.index)

	log.Printf("Writing %d bytes to bucket %s, object %s", len(data), c.bucket, fileName)

	wc := c.client.Bucket(c.bucket).Object(fileName).NewWriter(c.ctx)
	defer wc.Close()
	n, err = wc.Write(data)

	log.Printf("Wrote %d bytes to bucket %s, object %s", n, c.bucket, fileName)
	return n, err
}
