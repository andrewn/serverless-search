# Serverless Search

A tiny tool for searching logs stored in object storage in a massively parallel way, using Google Cloud Functions (serverless!) workers.

This is very much a work-in-progress!

# Useful Commands

## Deploy

```shell
$ gcloud --project gitlab-production functions deploy scan --entry-point ScanFunction --runtime go111 --trigger-topic scan-logs  --timeout=540 --memory=128MB
```

## Execute a parallel search across multiple objects (fanout)

This example shows how to run a query across all rails audit logs for 2018-12-03

```shell
go run cmd/fanout/fanout.go \
  -output-prefix test/andrew123 \
  -prefix "rails.audit/2018/12/03" \
  -search-string andrew
```

Let's break this down:

1. The results will appear in the [`gs://gitlab-log-scanner/`](https://console.cloud.google.com/storage/browser/gitlab-log-scanner/test/?project=gitlab-production&organizationId=769164969568) 
bucket, under the `test/andrew123` prefix (as specified by the `-output-prefix` parameter)
1. All log files that start with `rails.audit/2018/12/03` will be searched (as specified by the `-prefix` parameter)
1. Any line which contains the text `andrew` will be written to the destination bucket (as specified by the `-search-string` parameter)

Currently the only way to know whether the jobs are complete it by monitoring https://console.cloud.google.com/functions/details/us-central1/scan?project=gitlab-production,
in future we hope to make this easier.
