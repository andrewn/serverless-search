package search

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"cloud.google.com/go/storage"
)

// ScanFunction will scan a bucket for a search string
func ScanFunction(ctx context.Context, e Event) error {
	f := ScanEvent{}
	err := json.Unmarshal(e.Data, &f)
	if err != nil {
		return fmt.Errorf("json: parse: %v", err)
	}

	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Printf("error: scan unable to create client: %v", err)
		return err
	}

	err = Scan(ctx, storageClient, f)
	if err != nil {
		log.Printf("error: scan: %v", err)
		return err
	}

	return nil
}
