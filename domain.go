package search

// FanoutEvent holds event data from a Google Cloud Storage Event.
type FanoutEvent struct {
	InputBucket  string `json:"input_bucket"`
	Prefix       string `json:"prefix"`
	OutputBucket string `json:"output_bucket"`
	OutputPrefix string `json:"output_prefix"`
	SearchString string `json:"search_string"`
}

// ScanEvent holds event data from a Google Cloud Storage Event.
type ScanEvent struct {
	InputBucket  string `json:"input_bucket"`
	InputObject  string `json:"input_object"`
	OutputBucket string `json:"output_bucket"`
	OutputPrefix string `json:"output_prefix"`
	SearchString string `json:"search_string"`
}

// Event holds event data from a Google Cloud Storage Event.
type Event struct {
	Data []byte `json:"data"`
}
