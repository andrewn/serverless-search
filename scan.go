package search

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/storage"
)

const chunkSize = 1024 * 1024
const flushLimit = 4096

// Scan will scan a bucket for a search string
func Scan(ctx context.Context, storageClient *storage.Client, e ScanEvent) error {
	log.Printf("scan request starting with %+v", e)
	if e.InputBucket == "" || e.InputObject == "" || e.OutputBucket == "" || e.OutputPrefix == "" || len(e.SearchString) < 3 {
		err := fmt.Errorf("scan: invalid request: %+v", e)
		log.Printf("error: validation failure: %v", err)

		return err
	}

	err := scan(ctx, storageClient, e)
	if err != nil {
		log.Printf("error: scan: %v", err)
	}
	return err
}

func scan(ctx context.Context, client *storage.Client, e ScanEvent) error {
	outputObjectName := fmt.Sprintf("%s/%s", e.OutputPrefix, e.InputObject)
	writer := newChunkWriter(ctx, client, e.OutputBucket, outputObjectName)
	bufferedWriter := bufio.NewWriterSize(writer, chunkSize)

	err := scanToWriter(ctx, bufferedWriter, client, e.InputBucket, e.InputObject, e.SearchString)

	// Force a final flush before the deferred close
	err1 := bufferedWriter.Flush()
	if err1 != nil {
		log.Printf("error: scan: flush failed: %v", err1)
	}

	// First error takes preference
	if err != nil {
		return err
	}

	return err1
}

func scanToWriter(ctx context.Context, writer *bufio.Writer, client *storage.Client, bucket, object string, searchString string) error {
	log.Printf("scan: searching for %s in %s", searchString, object)

	rc, err := client.Bucket(bucket).Object(object).NewReader(ctx)
	if err != nil {
		return err
	}
	defer rc.Close()

	count := 0

	searchBytes := []byte(searchString)
	scanner := bufio.NewScanner(rc)
	for scanner.Scan() {
		if bytes.Contains(scanner.Bytes(), searchBytes) {
			count++

			_, err := writer.WriteString(scanner.Text())
			if err != nil {
				return err
			}

			// Try keep lines together in the same file
			if writer.Available() < flushLimit {
				err = writer.Flush()
				if err != nil {
					return err
				}
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	log.Printf("scan: completed search for %s in %s. Found %d matching lines", searchString, object, count)

	return nil
}
